-----

**温馨提示：切换至仓库的 [releases](https://gitee.com/yangrz/recompile_php_manual_zh/releases) 分支，可直接下载本人已编译好的 chm 手册文件**

-----

目前，从 PHP 官网 https://www.php.net/download-docs.php 下载的 chm 中文手册有两个大问题：

1. 索引不完整，中文标题的页面没有索引；
2. 搜索栏用不了，无论搜什么都搜不出来。

鉴于此，本人参考了网上制作 chm 的文档，自己重新编译一个版本。

在 Windows 系统下，chm 文件是通过微软的 [HTML Help Workshop](https://docs.microsoft.com/zh-cn/previous-versions/windows/desktop/htmlhelp/microsoft-html-help-downloads) 生成的。chm 文件可以用 7-zip 进行打开，解压出来后可得到原 html 文件、hhc 目录文件、hhk 索引文件。

本人用 7-zip 解压从 PHP 官网下载的 [php_enhanced_zh.chm](https://www.php.net/distributions/manual/php_enhanced_zh.chm) ，得到所有的 html，以及 `php_manual_zh.hhc`、`php_manual_zh.hhk`，打开这两个文件没有看到任何中文字符，猜测可能是解压的时候丢失了，或者原本官网打包就有问题。如果要重新制作 chm 手册，我们还缺少一个 hhp 后缀名的项目配置文件 。

这便是本人写的脚本，用于重新生成 `php_manual_zh.hhc`、`php_manual_zh.hhk`、`php_manual_zh.hhp`。

使用方法：

1. 下载仓库中的文件 **index.php** 保存至 web 目录
2. 在同目录下新建子目录 **php_enhanced_zh/**，并解压 **php_enhanced_zh.chm** 到这个子目录下
3. 通过浏览器访问这个 **index.php**，或者命令行执行 **php index.php**（耗时较长，耐心等待）
4. 用 HTML Help Workshop 打开生成的 **php_enhanced_zh/php_manual_zh.hhp**
5. 点击 HTML Help Workshop 工具栏的 “**Compile HTML file**” 按钮进行编译
6. 编译完成后，新的手册文件保存在 **php_enhanced_zh/php_enhanced_zh.chm**

> 本人博文：https://www.yangdx.com/2020/01/81.html
