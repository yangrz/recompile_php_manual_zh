<?php

error_reporting(E_ALL);
set_time_limit(0);
ini_set('display_errors', 'on');
date_default_timezone_set('Asia/Shanghai');

define('CHM_PATH', __DIR__ . '/php_enhanced_zh/');

/**
 * 搜索html文件
 * @return array
 */
function searchHtml()
{
    $pattern = CHM_PATH . 'res/*.html';
    $files = glob($pattern);
    return $files;
}

/**
 * 获取文件内容并转换为UTF-8编码
 * @param string $file
 * @return string
 */
function getFileWithUTF8($file)
{
    $content = file_get_contents($file);
    $content = iconv('gbk', 'utf-8', $content);
    return $content;
}

/**
 * 修复导航条
 * @param string $content
 * @return string
 */
function fixNavbar($content)
{
    $pattern = [
        '#<li style="float: left;"><a href="([^"]+)">\? (.*?)</a></li>#',
        '#<li style="float: right;"><a href="([^"]+)">(.*?) \?</a></li>#'
    ];
    $replacement = [
        '<li style="float: left;"><a href="\1">&laquo; \2</a></li>',
        '<li style="float: right;"><a href="\1">\2 &raquo;</a></li>'
    ];
    $content = preg_replace($pattern, $replacement, $content);
    return $content;
}

/**
 * 修复面包屑
 * @param string $content
 * @return string
 */
function fixBreadcrumbs($content)
{
    $search = '<ul class="breadcrumbs-container">';
    $replace = '<ul id="breadcrumbs-inner" class="breadcrumbs-container">';
    $content = str_replace($search, $replace, $content);
    return $content;
}

/**
 * 修复评论区代码2次换行的格式问题
 * @param string $content
 * @return string
 */
function fixPhpCodeBr($content)
{
    $content = preg_replace_callback(
        '#<div class="phpcode">\s*<code>.+?</code>\s*</div>#s',
        function ($matches) {
            return str_replace("<br />\r\n", '<br />', $matches[0]);
        },
        $content
    );

    return $content;
}

/**
 * 解析页面标题
 * @param string $content
 * @return string
 */
function parseTitle($content)
{
    $title = [];
    $pattern = '#<div class="refnamediv">(.+?)</div>#s';
    $matches = [];
    preg_match($pattern, $content, $matches);

    if (!$matches) {
        $pattern = '#<div id="layout-content"><div(.+?)(<(p|ul|table|dl|div)[ >]?)#s';
        preg_match($pattern, $content, $matches);
    }

    $subject = isset($matches[0]) ? $matches[0] : '';
    $patterns = [
        '#<h1([^>]*)>(.+?)</h1>#s',
        '#<h2([^>]*)>(.+?)</h2>#s',
        '#<h3([^>]*)>(.+?)</h3>#s'
    ];

    foreach ($patterns as $pattern) {
        preg_match_all($pattern, $subject, $matches);
        if ($matches[0]) {
            foreach ($matches[2] as $v) {
                $v = strip_tags($v);
                $v = trim($v);
                $v = preg_replace('#\s+#', ' ', $v);
                $title[] = $v;
            }
            break;
        }
    }

    $pattern = '#<title>(.+?)</title>#s';
    preg_match($pattern, $content, $matches);
    if ($matches) {
        $v = preg_replace('#\s+#', ' ', $matches[1]);
        $title[] = $v;
    }

    $title = array_unique($title);

    return $title;
}

/**
 * 以gbk编码保存文件内容
 * @param string $file
 * @param string $content
 */
function saveFileWithGBK($file, $content)
{
    $content = iconv('utf-8', 'gbk', $content);
    file_put_contents($file, $content);
}

/**
 * 重建目录文件
 * @param array $titles
 */
function buildHhcFile(&$titles)
{
    $hhcFile = CHM_PATH . 'php_manual_zh.hhc';
    $content = getFileWithUTF8($hhcFile);
    $pattern = '#<param name="Name" value="(.*?)">(?:\s*)<param name="Local" value="([^"]+)">(?:\s*)</OBJECT>(?:\s*)<(/?ul|LI)>#s';
    preg_match_all($pattern, $content, $matches);
    foreach ($matches[0] as $key => $value) {
        $oldTitle = $matches[1][$key];
        $shortPath = $matches[2][$key];
        $nextTag = $matches[3][$key];
        if ($oldTitle) {
            $title = preg_replace('#\s+#', ' ', $oldTitle);
        } else {
            $title = isset($titles[$shortPath]) ? $titles[$shortPath] : [];
            $title = isset($title[0]) ? $title[0] : '无标题';
        }
        $replace = str_replace('value="' . $oldTitle . '"', 'value="' . $title . '"', $value);
        if ($nextTag != 'ul') {
            //没有子节点的话用这个图标（默认是带问号的图标）
            $replace = str_replace('</OBJECT>', '<param name="ImageNumber" value="11"></OBJECT>', $replace);
        }
        $content = str_replace($value, $replace, $content);
    }
    saveFileWithGBK($hhcFile, $content);
}

/**
 * 重建索引文件
 * @param array $titles
 */
function buildHhkFile(&$titles)
{
    $hhkFile = CHM_PATH . 'php_manual_zh.hhk';
    $content = getFileWithUTF8($hhkFile);
    $content = preg_replace('#<ul>(.+)</ul>#s', '<ul>PLACEHOLDER</ul>', $content);
    $html = '';
    foreach ($titles as $shortPath => $title) {
        foreach ($title as $value) {
            $html .= '
                <LI><OBJECT type="text/sitemap">
                  <param name="Local" value="' . $shortPath . '">
                  <param name="Name" value="' . $value . '">
                </OBJECT>';
        }
    }
    $content = str_replace('PLACEHOLDER', $html, $content);
    saveFileWithGBK($hhkFile, $content);
}

/**
 * 生成项目配置文件
 * @param array $htmlFiles
 */
function buildHhpFile(&$htmlFiles)
{
    $date = date('Ymd');

    $content = <<<EOT
[OPTIONS]
Compatibility=1.1 or later
Compiled file=php_enhanced_zh.chm
Contents file=php_manual_zh.hhc
Default Window=phpchm
Default topic=res\index.html
Display compile progress=Yes
Full-text search=Yes
Index file=php_manual_zh.hhk
Language=0x804 中文(简体，中国)
Title=PHP中文手册

[WINDOWS]
phpchm="PHP中文手册 - {$date}","php_manual_zh.hhc","php_manual_zh.hhk","res\index.html","res\index.html",,,,,0x23520,,0x387e,,0x1000000,,,,,,0

[FILES]
EOT;

    foreach ($htmlFiles as $filePath) {
        $shortPath = str_replace([CHM_PATH, '/'], ['', '\\'], $filePath);
        $content .= "\n" . $shortPath;
    }
    $hhpFile = CHM_PATH . 'php_manual_zh.hhp';
    saveFileWithGBK($hhpFile, $content);
}

/**
 * 将css文件变量替换为具体的值
 * @return void
 */
function replaceCssVar()
{
    $vars = [];

    // 注意顺序不能换
    foreach (['base.css', 'medium.css'] as $item) {
        $files = glob(CHM_PATH . 'res/styles/*-' . $item);

        if (!$files) {
            continue;
        }

        $cssPath = $files[0];
        $cssContent = file_get_contents($cssPath);

        foreach ($vars as $key => $value) {
            $cssContent = str_replace("var($key)", $value, $cssContent);
        }

        preg_match('/:root \{(.+?)}/s', $cssContent, $match1);

        if (isset($match1[1])) {
            preg_match_all('/\s*(--.+?): (#\w+);\s*/', $match1[1], $match2);

            foreach ($match2[1] as $k => $v) {
                $vars[$v] = $match2[2][$k];
            }

            foreach ($vars as $key => $value) {
                $cssContent = str_replace("var($key)", $value, $cssContent);
            }

            file_put_contents($cssPath, $cssContent);
        }
    }
}

$startTime = time();
$titles = [];

echo "正在检索 html 文件...\n";

$htmlFiles = searchHtml();

if (!$htmlFiles) {
    echo "未检索到 html 文件，请下载手册原文件并解压至 " . CHM_PATH . " 目录\n";
    echo "下载地址 https://www.php.net/distributions/manual/php_enhanced_zh.chm\n";
    exit;
}

echo "开始处理 html 文件\n";
$count = count($htmlFiles);
$progress = 0;
foreach ($htmlFiles as $key => $filePath) {
    $content = getFileWithUTF8($filePath);
    $content = fixNavbar($content);
    $content = fixBreadcrumbs($content);
    $content = fixPhpCodeBr($content);
    $title = parseTitle($content);
    $shortPath = str_replace([CHM_PATH, '\\'], ['', '/'], $filePath);
    $titles[$shortPath] = $title;
    saveFileWithGBK($filePath, $content);
    $newProgress = floor(($key + 1) / $count * 100);
    if ($progress != $newProgress) {
        $progress = $newProgress;
        echo "\r当前进度 {$progress}%";
        if ($progress == 100) {
            echo "\n";
        }
    }
}

echo "替换 css 变量\n";
replaceCssVar();

echo "正在生成目录文件 php_manual_zh.hhc\n";
buildHhcFile($titles);

echo "正在生成索引文件 php_manual_zh.hhk\n";
buildHhkFile($titles);

echo "正在生成项目文件 php_manual_zh.hhp\n";
buildHhpFile($htmlFiles);

$useTime = time() - $startTime;
$minute = floor($useTime / 60);
$second = $useTime % 60;

echo "已完成，用时 {$minute} 分 {$second} 秒\n";
